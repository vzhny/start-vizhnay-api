# start.vizhnay.io API

A minimal API designed for use with start.vizhnay.io.

### Credit

Initial babel setup steps found [here](https://github.com/babel/example-node-server).

Nodemon + babel script key found [here](https://link.medium.com/3otVmCgGyT).
